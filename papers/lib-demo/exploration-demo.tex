% Options for packages loaded elsewhere
\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
%
\documentclass[
  sigconf]{acmart}
\usepackage{amsmath,amssymb}
\usepackage{lmodern}
\usepackage{iftex}
\ifPDFTeX
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provide euro and other symbols
\else % if luatex or xetex
%  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
\fi
% Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{xcolor}
\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  pdftitle={Representing Exploration in Metroidvania Games},
  pdfkeywords={exploration, graphs, player experience, metroidvania},
  hidelinks,
  pdfcreator={LaTeX via pandoc}}
\urlstyle{same} % disable monospaced font for URLs
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering
\setcopyright{acmlicensed}
\copyrightyear{2022}
\acmYear{2022}
\acmDOI{TODO}


\acmConference[PCG Workshop '22]{Procedural Content Generation Workshop}{September 5, 2022}{Athens, Greece}
\acmPrice{0.00}
\acmISBN{TOOD}

\begin{CCSXML}
<ccs2012>
   <concept>
       <concept_id>10010405.10010469.10010474</concept_id>
       <concept_desc>Applied computing~Media arts</concept_desc>
       <concept_significance>300</concept_significance>
       </concept>
   <concept>
       <concept_id>10011007.10011006.10011072</concept_id>
       <concept_desc>Software and its engineering~Software libraries and repositories</concept_desc>
       <concept_significance>300</concept_significance>
       </concept>
   <concept>
       <concept_id>10003120.10003121.10003122.10003332</concept_id>
       <concept_desc>Human-centered computing~User models</concept_desc>
       <concept_significance>300</concept_significance>
       </concept>
   <concept>
       <concept_id>10003120.10003121.10003126</concept_id>
       <concept_desc>Human-centered computing~HCI theory, concepts and models</concept_desc>
       <concept_significance>100</concept_significance>
       </concept>
 </ccs2012>
\end{CCSXML}

\ccsdesc[300]{Applied computing~Media arts}
\ccsdesc[300]{Software and its engineering~Software libraries and repositories}
\ccsdesc[300]{Human-centered computing~User models}
\ccsdesc[100]{Human-centered computing~HCI theory, concepts and models}

\author{Peter Mawhorter}
\email{pmawhort@wellesley.edu}
\orcid{0000-0001-7821-1973}
\affiliation{%
  \institution{Wellesley College}
  \streetaddress{106 Central Street}
  \city{Wellesley}
  \state{MA}
  \country{USA}
  \postcode{01545}
}

\author{Indira Ruslanova}
\email{indira.ruslanova@wellesley.edu}
\affiliation{%
  \institution{Wellesley College}
  \streetaddress{106 Central Street}
  \city{Wellesley}
  \state{MA}
  \country{USA}
  \postcode{01545}
}

\author{Ross Mawhorter}
\email{rmawhort@ucsc.edu}
\orcid{0000-0002-4735-010X}
\affiliation{%
  \institution{UC Santa Cruz}
  \streetaddress{1156 High Street}
  \city{Santa Cruz}
  \state{CA}
  \country{USA}
  \postcode{95064}
}
\ifLuaTeX
  \usepackage{selnolig}  % disable illegal ligatures
\fi
\usepackage[]{natbib}
\bibliographystyle{ACM-Reference-Format}

\title{Representing Exploration in Metroidvania Games}
\usepackage{etoolbox}
\makeatletter
\providecommand{\subtitle}[1]{% add subtitle to \maketitle
  \apptocmd{\@title}{\par {\large #1 \par}}{}{}
}
\makeatother
\subtitle{A demo of the \texttt{exploration} Python library}
%\author{}
\date{}

\begin{document}
\begin{abstract}
Exploration is a core gameplay element in many games, but it has a
special central place in the Metroidvania genre, where hidden powerups,
backtracking, and map-based navigation are genre-defining elements. At
the same time, space in Metroidvania games can be quite well represented
by an abstract topology (i.e., a graph of connections between rooms),
without needing to deal with the details of a grid-based or survey-type
models of spatial relationships. These two properties give rise to
unique challenges and opportunities in understanding the player
experience of exploration in these games. This demo will walk the
audience through \texttt{exploration}, an open-source Python library
available on PyPI which includes data structures for representing
exploration processes in discrete decision spaces including Metroidvania
games, as well as parsing routines for a journal format to easily
express exploration data by hand.
\end{abstract}
\keywords{exploration, graphs, player experience, metroidvania}
\maketitle



\hypertarget{metroidvania-games-and-exploration}{%
\section{Metroidvania Games and
Exploration}\label{metroidvania-games-and-exploration}}

``Metroidvania'' is a game genre based on the operational logics of
\emph{Super Metroid} \citep{super_metroid} and \emph{Castlevania:
Symphony of the Night}
\citep{castlevania_symphony_of_the_night}:\footnote{Note that the
  original \emph{Metroid} is at the edge of the genre, whereas the
  original \emph{Castlevania} is not even part of the genre. See
  \citep{metroidvania_com} for an explanation of the origins of the
  term.} an explorable space that includes barriers which can be passed
after discovering permanent upgrades, secret areas with optional
upgrades, and an in-game map to aid in exploration. In contrast to
``open-world'' games, space is constrained to a series of rooms with
specific paths between them; it can thus be cleanly abstracted into a
topological representation, although spatial relationships and metrics
are still important for using the map system.

In these games and more recent examples like \emph{Metroid Prime} (in
3D) \citep{metroid_prime} and \emph{Ori and the Blind Forest} (which
does away with room transitions) \citep{ori_and_the_blind_forest} the
excitement of exploration and discovery is central, even though combat
and platforming challenges are also important. For many such games, fans
have also created \emph{randomizers} \citep{jewett2021game} which
scramble the item locations or in some cases room connections, which
pose their own mapping challenges. As groundwork for future research
into human exploration processes in Metroidvania games to support
principled level generation, we have developed a library titled
\texttt{exploration} to represent exploration processes.

Existing work on exploration in games has focused on open-ended 2D and
3D environments, usually using maps to represent space (see e.g.,
\citep[
\citet{dahlskog2015patterns}]{osborn2021mappyland, si2017believable, stahlke2020synthesizing}).
At the same time, some work in both procedural content generation and
reinforcement learning has emphasized graph-based topological
representations, although these works have not taken the human
exploration process as their main focus (see e.g.,
\citep{dormans2010adventures, smith2018graph, zhan2018taking}). The
visualization system of \citep{moura2011visualizing} is close to what we
have created, because it visualizes player movement through a continuous
space as movement on a discrete graph of regions, although it does not
reason about player exploration processes.

In a similar dichotomy, the literature on the psychology of exploration
makes a distinction between route-based and survey-like spatial
knowledge, often assuming that route-based knowledge is an earlier stage
of knowledge development than survey-like knowledge (see
\citep{jamshidi2021narrative} for a review and
\citep{peer2021structuring} on the route/survey question). The lack of
an accessible and established format for capturing discrete player
exploration traces led to the creation of our library. Although the
formats used by \citep{dormans2010adventures, brown2016boss} for
representing game spaces are close, they do not capture the process of
exploration (only the structure of the map), and in any case they do not
come with open-source tools to support creating and reasoning about
maps.

\hypertarget{the-exploration-library}{%
\subsection{\texorpdfstring{The \texttt{exploration}
Library}{The exploration Library}}\label{the-exploration-library}}

Our demo will show off the capabilities of
\href{https://pypi.org/project/exploration/}{the \texttt{exploration}
Python library}, which is open-source under a 3-clause BSD license and
available via the Python Package Index (PyPI). Running:

\texttt{python~-m~pip~install~exploration}

should install a copy if you have Python version 3.7 or later. The
library includes three main features:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  An interactive representation of Metroidvania-style maps as
  multi-digraphs with specialized node and edge labels. This is
  implemented by the \texttt{core.DecisionGraph} class.
\item
  A abstract representation of an exploration process as a sequence of
  such decision graphs, implemented by the \texttt{core.Exploration}
  class.
\item
  A parser for a plain-text journal format which can produce
  \texttt{Exploration} objects given a human read- and write-able
  journal of observations during play. This is implemented by the
  \texttt{journal.JournalObserver} class.
\end{enumerate}

The \texttt{Exploration} class stores a list of discrete time steps,
with a decision graph, a position in that graph, a transition taken, and
a game state corresponding to each time step. The individual decision
graphs support tags and annotations as well as requirements for and
effects of transitions, allowing rich representation of Metroidvania
staples like powerups that allow traversal of previously-blocked
transitions, and mechanisms or world events which likewise open up or
close off certain transitions. In fact, with the addition of a planned
visualization layer and a bit of interactivity, the map format itself
could act as a kind of game engine, although things like combat or
platforming challenges would only be represented via shallow
abstractions.\footnote{The game \emph{Maptroid} (which does not have any
  platforming or combat) provides an example of what could be achieved
  by adding simple graphics and basic controls to the map format
  \citep{maptroid}.}

The design choices that we hope to discuss are:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  The choice of a graph-based rather than grid-based map format.

  \begin{itemize}
  \tightlist
  \item
    This opens up new perspectives for analysis.
  \item
    It can be unwieldy when dealing with open spaces where routes exist
    between most locations.
  \end{itemize}
\item
  The choice of a multi-digraph as the graph format, and considerations
  of how spatial relations can be retained in this format.

  \begin{itemize}
  \tightlist
  \item
    Multiple connections between nodes can be used to represent multiple
    paths (possible with different restrictions) between locations.
  \item
    Self-loops represent actions that can be taken within a location.
  \item
    Spatial relations can be annotated, but this annotation is
    potentially onerous.
  \end{itemize}
\item
  The choice to represent unexplored spaces as tagged nodes in the
  graph.

  \begin{itemize}
  \tightlist
  \item
    This allows annotating things like multiple connections which you
    know lead to the same (still-not-explored) location.
  \end{itemize}
\item
  The choice to record a sequence of entire graphs, rather than simply
  tracking position information in a single canonical graph.

  \begin{itemize}
  \tightlist
  \item
    This allows for transitions which alter the graph structure (e.g., a
    cutscene where the level layout changes).
  \item
    It also supports annotating mistaken beliefs that are later
    corrected.
  \end{itemize}
\end{enumerate}

Each of these design decisions has far-reaching consequences for a
system that hopes to represent exploration traces, and several of those
are things we've learned the hard way during the development process.

\hypertarget{acknowledgements}{%
\subsection{Acknowledgements}\label{acknowledgements}}

This work was done on land stolen from the Massachusett, Wampanoag,
Nipmuck, Uypi, Amah Mutsun, and Chumash peoples, and the authors are
grateful for their stewardship of that land in the past, in the present,
and in the future. The history of this land is important because this
work could not have happened without its use. We acknowledge that we
directly benefit from this ongoing injustice.

  \bibliography{refs.bib}

\end{document}
