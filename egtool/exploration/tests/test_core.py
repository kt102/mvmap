"""
Authors: Peter Mawhorter
Consulted:
Date: 2022-3-12
Purpose: Tests for the core functionality and types.
"""

import json

import pytest

from .. import core


def test_Requirements() -> None:
    """
    Multi-method test for `exploration.core.Requirement` and sub-classes.
    """
    # Tests of comparison
    r: core.Requirement = core.ReqAll([
        core.ReqAny([core.ReqPower('p1'), core.ReqPower('p2')]),
        core.ReqTokens('key', 1)
    ])
    r2 = core.ReqAll([
        core.ReqAny([core.ReqPower('p1'), core.ReqPower('p2')]),
        core.ReqTokens('key', 1)
    ])
    assert r == r2
    assert core.ReqNothing() == core.ReqNothing()
    assert core.ReqImpossible() == core.ReqImpossible()

    # Tests of satisfied
    assert r.satisfied({'powers': {'p1'}, 'tokens': {'key': 1}})
    assert not r.satisfied({'powers': set(), 'tokens': {'key': 1}})
    assert not r.satisfied({'powers': {'p1'}, 'tokens': {'key': 0}})
    assert not r.satisfied({'powers': {'p1'}, 'tokens': {}})
    assert r.satisfied({'powers': {'p1', 'p2'}, 'tokens': {'key': 2}})

    r = core.ReqAny([
        core.ReqAll([core.ReqPower('p1'), core.ReqPower('p2')]),
        core.ReqTokens('key', 3)
    ])
    assert r.satisfied({'powers': {'p1'}, 'tokens': {'key': 3}})
    assert not r.satisfied({'powers': set(), 'tokens': {'key': 1}})
    assert not r.satisfied({'powers': {'p2'}, 'tokens': {'key': 0}})
    assert not r.satisfied({'powers': {'p1'}, 'tokens': {}})
    assert r.satisfied({'powers': {'p1', 'p2'}, 'tokens': {}})
    assert r.satisfied({'powers': {'p1', 'p2'}, 'tokens': {'key': 2}})
    assert r.satisfied({'powers': {'p1', 'p2'}, 'tokens': {'key': 5}})

    assert core.hasPowerOrEquivalent('p3', {'powers': {'p3'}})
    assert core.hasPowerOrEquivalent(
        'p3',
        {'powers': {'p3'}},
        {'p2': {core.ReqPower('p3')}}
    )
    assert core.hasPowerOrEquivalent(
        'p2',
        {'powers': {'p3'}},
        {'p2': {core.ReqPower('p3')}}
    )
    assert core.ReqPower('p2').satisfied(
        {'powers': {'p3'}},
        {'p2': {core.ReqPower('p3')}}
    )
    assert r.satisfied(
        {'powers': {'p1', 'p3'}},
        {'p2': {core.ReqPower('p3')}}
    )

    assert not r.satisfied(
        {'powers': {'p1', 'p3'}},
        {'p2': {core.ReqPower('p2')}}
    )

    r = core.ReqImpossible()
    assert not r.satisfied({})

    r = core.ReqNothing()
    assert r.satisfied({})

    r = core.ReqNot(core.ReqAll([core.ReqPower('p1'), core.ReqPower('p2')]))
    assert r.satisfied({'powers': set()})
    assert r.satisfied({'powers': {'p1'}})
    assert r.satisfied({'powers': {'p2'}})
    assert not r.satisfied({'powers': {'p1', 'p2'}})

    # Tests of parsing:
    parsed = core.Requirement.parse('a')
    assert parsed == core.ReqPower('a')

    parsed = core.Requirement.parse('(a)')
    assert parsed == core.ReqPower('a')

    parsed = core.Requirement.parse('a*5')
    assert parsed == core.ReqTokens('a', 5)

    parsed = core.Requirement.parse('((a*5))')
    assert parsed == core.ReqTokens('a', 5)

    parsed = core.Requirement.parse('(a|b)&c*3')
    assert parsed == core.ReqAll([
        core.ReqAny([core.ReqPower('a'), core.ReqPower('b')]),
        core.ReqTokens('c', 3)
    ])

    parsed = core.Requirement.parse(' ( a | b )\t& c * 3 ')
    assert parsed == core.ReqAll([
        core.ReqAny([core.ReqPower('a'), core.ReqPower('b')]),
        core.ReqTokens('c', 3)
    ])

    parsed = core.Requirement.parse('a|(b&c*3)')
    assert parsed == core.ReqAny([
        core.ReqPower('a'),
        core.ReqAll([core.ReqPower('b'), core.ReqTokens('c', 3)]),
    ])

    parsed = core.Requirement.parse('a|b&c*3')
    assert parsed == core.ReqAny([
        core.ReqPower('a'),
        core.ReqAll([core.ReqPower('b'), core.ReqTokens('c', 3)]),
    ])

    parsed = core.Requirement.parse('a&b|c*3')
    assert parsed == core.ReqAny([
        core.ReqAll([core.ReqPower('a'), core.ReqPower('b')]),
        core.ReqTokens('c', 3),
    ])

    parsed = core.Requirement.parse('a|b|c')
    assert parsed == core.ReqAny([
        core.ReqPower('a'),
        core.ReqPower('b'),
        core.ReqPower('c'),
    ])

    parsed = core.Requirement.parse('a&b&c&d')
    assert parsed == core.ReqAll([
        core.ReqPower('a'),
        core.ReqPower('b'),
        core.ReqPower('c'),
        core.ReqPower('d'),
    ])

    parsed = core.Requirement.parse('a&b|c&d')
    assert parsed == core.ReqAny([
        core.ReqAll([
            core.ReqPower('a'),
            core.ReqPower('b')
        ]),
        core.ReqAll([
            core.ReqPower('c'),
            core.ReqPower('d')
        ]),
    ])

    parsed = core.Requirement.parse('a&-b|-c&d')
    assert parsed == core.ReqAny([
        core.ReqAll([
            core.ReqPower('a'),
            core.ReqNot(core.ReqPower('b'))
        ]),
        core.ReqAll([
            core.ReqNot(core.ReqPower('c')),
            core.ReqPower('d')
        ]),
    ])

    parsed = core.Requirement.parse('-a&b&c')
    assert parsed == core.ReqAll([
        core.ReqNot(core.ReqPower('a')),
        core.ReqPower('b'),
        core.ReqPower('c')
    ])

    parsed = core.Requirement.parse('-(a&b)&c')
    assert parsed == core.ReqAll([
        core.ReqNot(
            core.ReqAll([core.ReqPower('a'), core.ReqPower('b')])
        ),
        core.ReqPower('c')
    ])

    parsed = core.Requirement.parse('-c*3')
    assert parsed == core.ReqNot(core.ReqTokens('c', 3))

    parsed = core.Requirement.parse('X')
    assert parsed == core.ReqImpossible()

    parsed = core.Requirement.parse('O')
    assert parsed == core.ReqNothing()

    parsed = core.Requirement.parse('X|a')
    assert parsed == core.ReqAny([
        core.ReqImpossible(),
        core.ReqPower('a')
    ])

    with pytest.raises(ValueError):
        parsed = core.Requirement.parse('a*3*2')

    with pytest.raises(ValueError):
        parsed = core.Requirement.parse('(a):2')

    with pytest.raises(ValueError):
        parsed = core.Requirement.parse('(a|b&c):3')

    with pytest.raises(ValueError):
        parsed = core.Requirement.parse('a|&b')

    with pytest.raises(ValueError):
        parsed = core.Requirement.parse('(a|b')

    with pytest.raises(ValueError):
        parsed = core.Requirement.parse('a|b)')

    with pytest.raises(ValueError):
        parsed = core.Requirement.parse('a*-3')

    with pytest.raises(ValueError):
        parsed = core.Requirement.parse('a-b')

    with pytest.raises(ValueError):
        parsed = core.Requirement.parse('a*-b')


def test_DecisionGraph() -> None:
    "Multi-method test for `exploration.core.DecisionGraph`."
    m = core.DecisionGraph()
    m.addDecision('a')
    m.addDecision('b')
    m.addDecision('c')
    assert len(m) == 3
    assert set(m) == {'a', 'b', 'c'}

    m.addTransition('a', 'East', 'b', 'West')
    m.addTransition('a', 'Northeast', 'c')
    m.addTransition('c', 'Southeast', 'b')

    assert m.destinationsFrom('a') == {'East': 'b', 'Northeast': 'c'}
    assert m.destinationsFrom('b') == {'West': 'a'}
    assert m.destinationsFrom('c') == {'Southeast': 'b'}

    assert m.getReciprocal('a', 'East') == 'West'
    assert m.getReciprocal('a', 'Northeast') is None
    assert m.getReciprocal('b', 'Southwest') is None
    assert m.getReciprocal('c', 'Southeast') is None

    m.addUnexploredEdge('a', 'South')
    m.addUnexploredEdge('b', 'East')
    m.addUnexploredEdge('c', 'North')

    assert len(m) == 6
    assert set(m) == {'a', 'b', 'c', '_u.0', '_u.1', '_u.2'}
    assert (
        m.destinationsFrom('a')
     == {'East': 'b', 'Northeast': 'c', 'South': '_u.0'}
    )
    assert (m.destinationsFrom('b') == {'West': 'a', 'East': '_u.1'})
    assert (m.destinationsFrom('c') == {'Southeast': 'b', 'North': '_u.2'})

    m.replaceUnexplored('c', 'North', 'd', 'South')
    assert m.destinationsFrom('c') == {'Southeast': 'b', 'North': 'd'}
    assert len(m) == 6
    assert set(m) == {'a', 'b', 'c', '_u.0', '_u.1', 'd'}

    m.addTransition('d', 'West', 'a', 'North')
    assert (
        m.destinationsFrom('a')
     == {'East': 'b', 'Northeast': 'c', 'South': '_u.0', 'North': 'd'}
    )
    assert (
        m.destinationsFrom('d')
     == {'West': 'a', 'South': 'c'}
    )

    with pytest.raises(core.MissingDecisionError):
        _ = m.destinationsFrom('z')

    with pytest.raises(core.TransitionCollisionError):
        m.addTransition('a', 'East', 'b', 'West')

    with pytest.raises(core.TransitionCollisionError):
        m.addTransition('c', 'East', 'b', 'West')

    with pytest.raises(core.TransitionCollisionError):
        m.addUnexploredEdge('a', 'East')

    with pytest.raises(core.TransitionCollisionError):
        m.addUnexploredEdge('c', 'North')

    with pytest.raises(core.MissingTransitionError):
        m.replaceUnexplored('a', 'Up', 'z')

    with pytest.raises(core.UnknownDestinationError):
        m.replaceUnexplored('a', 'East', 'z')

    assert m.destinationsFrom('c') == {'Southeast': 'b', 'North': 'd'}

    m.addTransition('a', 'EastBelow', 'b', 'WestBelow')
    assert (m.destinationsFrom('a') == {
        'East': 'b',
        'EastBelow': 'b',
        'Northeast': 'c',
        'South': '_u.0',
        'North': 'd'
    })
    assert (
        m.destinationsFrom('b')
     == {'West': 'a', 'WestBelow': 'a', 'East': '_u.1'}
    )

    # Two edges that could be but are not reciprocals
    m.addTransition('d', 'East', 'b')
    m.addTransition('b', 'North', 'd')
    assert (
        m.destinationsFrom('b')
     == {'West': 'a', 'WestBelow': 'a', 'East': '_u.1', 'North': 'd'}
    )
    assert (
        m.destinationsFrom('d')
     == {'West': 'a', 'South': 'c', 'East': 'b'}
    )
    assert m.getReciprocal('b', 'North') is None
    assert m.getReciprocal('d', 'East') is None

    # Establish a reciprocal relationship
    m.setReciprocal('b', 'North', 'East')
    assert m.getReciprocal('b', 'North') == 'East'
    assert m.getReciprocal('d', 'East') == 'North'

    with pytest.raises(core.MissingDecisionError):
        m.setReciprocal('z', 'Nope', 'None')

    with pytest.raises(core.MissingTransitionError):
        m.setReciprocal('b', 'Nope', 'None')

    # Remove the reciprocal relationship again (from the other side)
    m.setReciprocal('d', 'East', None)
    assert m.getReciprocal('b', 'North') is None
    assert m.getReciprocal('d', 'East') is None

    with pytest.raises(core.InvalidDestinationError):
        m.setReciprocal('b', 'North', 'West')

    with pytest.raises(core.MissingTransitionError):
        m.setReciprocal('b', 'North', 'None')

    assert (m.isUnknown("_u.0"))
    assert (m.isUnknown("_u.1"))
    assert (m.isUnknown("a") is False)
    assert (m.isUnknown("d") is False)

    assert (
        json.dumps(m.textMapObj(), indent=4)
     == """\
{
    "a::East": {
        "b::West": "a",
        "b::East": {
            "_u.1::return": "b"
        },
        "b::WestBelow": "a",
        "b::North": {
            "d::South": {
                "c::Southeast": "b",
                "c::North": "d"
            },
            "d::West": "a",
            "d::East": "b"
        }
    },
    "a::Northeast": "c",
    "a::South": {
        "_u.0::return": "a"
    },
    "a::North": "d",
    "a::EastBelow": "b"
}"""
    )

    assert (
        json.dumps(
            m.textMapObj(
                explorationOrder=(
                    'a',
                    [
                        'East',
                        'West',
                        'South',
                        'return',
                        'Northeast',
                        'Southeast',
                        'North',
                    ]
                )
            ),
            indent=4
        )
     == """\
{
    "a::East": {
        "b::West": "a",
        "b::North": {
            "d::South": {
                "c::Southeast": "b",
                "c::North": "d"
            },
            "d::West": "a",
            "d::East": "b"
        },
        "b::East": {
            "_u.1::return": "b"
        },
        "b::WestBelow": "a"
    },
    "a::South": {
        "_u.0::return": "a"
    },
    "a::Northeast": "c",
    "a::North": "d",
    "a::EastBelow": "b"
}"""
    )

    m.addEnding('d', 'failure')
    assert set(m) == {'a', 'b', 'c', 'd', '_u.0', '_u.1', '_e:failure'}
    assert (
        m.destinationsFrom('d')
     == {'West': 'a', 'South': 'c', 'East': 'b', '_e:failure': '_e:failure'}
    )
    assert m.destinationsFrom('_e:failure') == {}


def test_DGTagsAndAnnotations() -> None:
    """
    Test for tagging decisions and transitions in an
    `exploration.core.DecisionGraph`.
    """
    m = core.DecisionGraph()
    m.addDecision('a')
    m.addDecision('b', {'grass'})
    m.addDecision('c', {'water', 'big'}, ["This is a note."])
    m.addTransition('a', 'East', 'b', 'West')
    m.addTransition(
        'a', 'South', 'c', 'North',
        {'green'}, ["Requires green key"],
        {'blue'}, ["Requires blue key"]
    )

    assert m.decisionTags('a') == set()
    assert m.decisionTags('b') == {'grass'}
    assert m.decisionTags('c') == {'water', 'big'}
    assert m.transitionTags('a', 'East') == set()
    assert m.transitionTags('a', 'South') == {'green'}
    assert m.transitionTags('b', 'West') == set()
    assert m.transitionTags('c', 'North') == {'blue'}

    assert m.decisionAnnotations('a') == []
    assert m.decisionAnnotations('b') == []
    assert m.decisionAnnotations('c') == ["This is a note."]
    assert m.transitionAnnotations('a', 'East') == []
    assert m.transitionAnnotations('a', 'South') == ["Requires green key"]
    assert m.transitionAnnotations('b', 'West') == []
    assert m.transitionAnnotations('c', 'North') == ["Requires blue key"]

    m.tagDecision('a', 'grass')
    assert m.decisionTags('a') == {'grass'}

    m.untagDecision('b', 'grass')
    assert m.decisionTags('b') == set()

    m.annotateDecision('a', "Starting location.")
    assert m.decisionAnnotations('a') == ["Starting location."]

    m.annotateDecision('c', "Blue key here.")
    assert m.decisionAnnotations('c') == ["This is a note.", "Blue key here."]

    assert m.decisionAnnotations('b') == []

    m.tagTransition('a', 'East', 'open')
    assert m.transitionTags('a', 'East') == {'open'}
    m.tagTransition('a', 'South', 'open')
    assert m.transitionTags('a', 'South') == {'green', 'open'}
    m.untagTransition('a', 'South', 'green')
    assert m.transitionTags('a', 'South') == {'open'}

    m.annotateTransition('c', 'North', "This was difficult.")
    assert (
        m.transitionAnnotations('c', 'North')
     == ['Requires blue key', 'This was difficult.']
    )
    cna = m.transitionAnnotations('c', 'North')
    cna.clear()
    cna.append('hi')
    assert m.transitionAnnotations('c', 'North') == ['hi']

    with pytest.raises(core.MissingTransitionError):
        _ = m.transitionAnnotations('a', 'West')


def test_Exploration() -> None:
    "Multi-method test for `exploration.core.Exploration`."
    e = core.Exploration()

    assert len(e.graphAtStep(0)) == 0

    with pytest.raises(core.MissingDecisionError):
        _ = e.positionAtStep(0)

    assert e.getPositionAtStep(0) is None

    assert e.transitionAtStep(0) is None

    assert len(e.currentGraph()) == 0

    with pytest.raises(core.MissingDecisionError):
        e.currentPosition() is None

    assert e.getCurrentPosition() is None

    situation: core.Situation = e.currentSituation()

    assert len(situation.graph) == 0
    assert situation.position is None
    assert situation.state == {}
    assert situation.transition is None

    assert len(e.graphs) == 1
    assert len(e.positions) == 1
    assert len(e.transitions) == 0
    assert len(e) == 1

    e.start('a', ['North', 'East', 'South'])

    assert len(e) == 2
    assert len(e.currentGraph()) == 4
    assert set(e.currentGraph()) == {'a', '_u.0', '_u.1', '_u.2'}
    assert e.currentPosition() == 'a'
    assert e.transitions == ['_START_']
    s = e.currentSituation()
    assert set(s.graph) == {'a', '_u.0', '_u.1', '_u.2'}
    assert s.position == 'a'
    assert s.state == {}
    assert s.transition is None

    e.explore('East', 'b', ['North', 'South'], 'West')
    e.currentGraph().removeTransition('_u.3', 'return') # truly one-way

    assert len(e) == 3
    s = e.situationAtStep(1)
    assert set(s.graph) == {'a', '_u.0', '_u.1', '_u.2'}
    assert s.position == 'a'
    assert s.state == {}
    assert s.transition == 'East'
    s = e.currentSituation()
    assert set(s.graph) == {'a', '_u.0', '_u.2', 'b', '_u.3', '_u.4'}
    assert s.position == 'b'
    assert s.state == {}
    assert s.transition is None
    assert (s.graph.destinationsFrom('a') == {
        'North': '_u.0',
        'East': 'b',
        'South': '_u.2'
    })
    assert (s.graph.destinationsFrom('b') == {
        'North': '_u.3',
        'West': 'a',
        'South': '_u.4'
    })
    assert (s.graph.destinationsFrom('_u.3') == {})
    assert (s.graph.destinationsFrom('_u.4') == {'return': 'b'})

    with pytest.raises(core.UnknownDestinationError):
        e.returnTo('West', 'a', 'North')

    with pytest.raises(core.UnknownDestinationError):
        e.returnTo('West', 'a', 'East')
        # (would need to use retrace instead)

    e.explore('North', 'c', ['West'], None)

    assert len(e) == 4
    s = e.situationAtStep(1)
    assert set(s.graph) == {'a', '_u.0', '_u.1', '_u.2'}
    assert s.position == 'a'
    assert s.state == {}
    assert s.transition == 'East'
    s = e.situationAtStep(2)
    assert set(s.graph) == {'a', '_u.0', '_u.2', 'b', '_u.3', '_u.4'}
    assert s.position == 'b'
    assert s.state == {}
    assert s.transition == 'North'
    assert (s.graph.destinationsFrom('a') == {
        'North': '_u.0',
        'East': 'b',
        'South': '_u.2'
    })
    assert (s.graph.destinationsFrom('b') == {
        'North': '_u.3',
        'West': 'a',
        'South': '_u.4'
    })
    s = e.situationAtStep(3)
    assert set(s.graph) == {'a', '_u.0', '_u.2', 'b', '_u.4', 'c', '_u.5'}
    assert s.position == 'c'
    assert s.state == {}
    assert s.transition is None
    assert (s.graph.destinationsFrom('a') == {
        'North': '_u.0',
        'East': 'b',
        'South': '_u.2'
    })
    assert (s.graph.destinationsFrom('b') == {
        'North': 'c',
        'West': 'a',
        'South': '_u.4'
    })
    assert s.graph.destinationsFrom('c') == {'West': '_u.5'}
    assert s.graph.destinationsFrom('_u.0') == {'return': 'a'}
    assert s.graph.destinationsFrom('_u.2') == {'return': 'a'}
    assert s.graph.destinationsFrom('_u.4') == {'return': 'b'}
    assert s.graph.destinationsFrom('_u.5') == {'return': 'c'}
    assert s.graph.degree('a') == 6
    assert s.graph.degree('b') == 5
    assert s.graph.degree('c') == 3
    assert s.graph.degree('_u.5') == 2

    e.explore('West', 'd', [], 'East')

    assert len(e) == 5
    s = e.situationAtStep(4)
    assert set(s.graph) == {'a', '_u.0', '_u.2', 'b', '_u.4', 'c', 'd'}
    assert s.position == 'd'
    assert s.state == {}
    assert s.transition is None
    assert s.graph.destinationsFrom('c') == {'West': 'd'}
    assert s.graph.destinationsFrom('d') == {'East': 'c'}
    assert s.graph.degree('c') == 3
    assert s.graph.degree('d') == 2

    # Can't return if there's no outgoing edge yet
    with pytest.raises(core.MissingTransitionError):
        e.returnTo('South', 'a', 'North')

    with pytest.raises(core.UnknownDestinationError):
        e.returnTo('East', 'a', 'North')

    # Add the edge and then we can use it to return
    g = e.currentGraph()
    g.addUnexploredEdge('d', 'South')
    assert set(g) == {'a', '_u.0', '_u.2', 'b', '_u.4', 'c', 'd', '_u.6'}
    e.returnTo('South', 'a', 'North')

    assert len(e) == 6
    s = e.situationAtStep(5)
    assert set(s.graph) == {'a', '_u.2', 'b', '_u.4', 'c', 'd'}
    assert s.position == 'a'
    assert s.state == {}
    assert s.transition is None
    assert s.graph.destinationsFrom('a') == {
        'East': 'b',
        'North': 'd',
        'South': '_u.2'
    }
    assert s.graph.destinationsFrom('d') == {'East': 'c', 'South': 'a'}
    assert s.graph.degree('c') == 3
    assert s.graph.degree('d') == 4
    assert s.graph.degree('a') == 6

    e.wait('message')

    assert len(e) == 7
    s = e.situationAtStep(6)
    assert set(s.graph) == {'a', '_u.2', 'b', '_u.4', 'c', 'd'}
    assert s.position == 'a'
    assert s.state == {}
    assert s.transition is None
    wm = e.transitionAtStep(5)
    assert wm == "..:message"

    e.takeAction(
        'powerUp',
        effects=[
            core.effect(gain='power'),
            core.effect(gain=('token', 2)),
        ]
    )

    assert len(e) == 8
    s = e.situationAtStep(7)
    assert set(s.graph) == {'a', '_u.2', 'b', '_u.4', 'c', 'd'}
    assert s.position == 'a'
    assert s.state == {'powers': {'power'}, 'tokens': {'token': 2}}
    assert s.transition is None
    am = e.transitionAtStep(6)
    assert am == "powerUp"
    assert (s.graph.destinationsFrom('a') == {
        'North': 'd',
        'East': 'b',
        'South': '_u.2',
        'powerUp': 'a'
    })

    e.retrace('East')

    assert len(e) == 9
    s = e.situationAtStep(8)
    pt = e.transitionAtStep(7)
    assert pt == "East"
    assert set(s.graph) == {'a', '_u.2', 'b', '_u.4', 'c', 'd'}
    assert s.position == 'b'
    assert s.state == {'powers': {'power'}, 'tokens': {'token': 2}}
    assert s.transition is None

    e.warp('d', 'teleport', effects=[core.effect(lose=('token', 1))])

    assert len(e) == 10
    s = e.situationAtStep(9)
    pt = e.transitionAtStep(8)
    assert pt == "~~:teleport"
    assert set(s.graph) == {'a', '_u.2', 'b', '_u.4', 'c', 'd'}
    assert s.position == 'd'
    assert s.state == {'powers': {'power'}, 'tokens': {'token': 1}}
    assert s.transition is None
    assert (s.graph.destinationsFrom('a') == {
        'North': 'd',
        'East': 'b',
        'South': '_u.2',
        'powerUp': 'a'
    })
    assert (s.graph.destinationsFrom('b') == {
        'North': 'c',
        'West': 'a',
        'South': '_u.4'
    })
    assert (s.graph.destinationsFrom('c') == {'West': 'd'})
    assert (s.graph.destinationsFrom('d') == {'East': 'c', 'South': 'a'})


def test_exploring_with_zones() -> None:
    """
    A test for exploring with zones being applied.
    """
    e = core.Exploration()

    e.start('start', zone='zone')
    e.observe('transition')
    e.explore('transition', 'room')

    g = e.currentGraph()
    assert g.zoneParents('start') == {'zone'}
    assert g.zoneParents('room') == {'zone'}

    e.observe('out', 'down')
    unknown = g.destination('room', 'down')
    g.renameDecision(unknown, 'fourth_room')
    assert g.isUnknown('fourth_room')
    e.explore('out', 'another_room', [], 'back', 'zone2')
    e.retrace('back')
    e.explore('down', 'fourth_room', 'up')

    g = e.currentGraph()
    assert g.zoneParents('start') == {'zone'}
    assert g.zoneParents('room') == {'zone'}
    assert g.zoneParents('another_room') == {'zone2'}
    assert g.zoneParents('fourth_room') == {'zone'}
