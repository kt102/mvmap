"""
- Authors: Nissi, Jada, Rachel
- Consulted: Peter Mawhorter
- Date: 2022-12-5
- Purpose: Tests for exploration analysis.
"""
# from .. import analysis, journal, core <--- original way of importing
# that wasn't working, changing the .. to exploration is a quick fix,
# but things might turn wonky if it stays like this
from exploration import analysis, journal, core


#Part of Rachel's journal:
JOURNAL = """
S Start Start_room
zz Starting_region
A gain attack
o right
o left
x left coin_room sr
o up above_coin_room fall
  #! q ?wall_kick_jumps  # Note: the leading '?' is new to indicate an
    # unknown requirement,, could not parse
t sr
x right platforms rp
o up
o right
x up dangerplat ud
"""

BABY_JOURNAL = """
S Start
A gain jump
A gain attack
n button check
zz Wilds
o up
q _flight
o left
x left left_nook right
a geo_rock
At gain geo*15
At deactivate
o up
q _tall_narrow
t right
o right
q attack
"""

BABY_JOURNAL_2 = """
S Start
A gain jump
A gain attack
n button check
zz Wilds
o up
q _flight
o left
x left left_nook right
"""


def test_getNumActions():
    """
    Test to make sure the number of actions in the whole graph
    at each step is accurate.
    """
    ex = journal.convertJournal(BABY_JOURNAL)
    assert analysis.getNumActions(ex) == 1
    assert analysis.getNumActions(ex.getGraphAtStep(0)) == 0
    assert analysis.getNumActions(ex.getGraphAtStep(1)) == 0
    assert analysis.getNumActions(ex.getGraphAtStep(2)) == 1


def test_CreateExploration():
    """
    Simple test to make sure we can create an exploration object for
    other tests in this file.
    """
    ex = journal.convertJournal(JOURNAL)
    assert len(ex) == 6
    assert ex.getPositionAtStep(0) is None
    assert ex.getPositionAtStep(1) == "Start"

    assert bool(ex.graphAtStep(1)) is True


def test_describeProgress():
    """
    Tests the `describeProgress` function.
    """
    import pytest
    pytest.xfail("Not finished yet.")

    e1 = journal.convertJournal(BABY_JOURNAL)
    e2 = journal.convertJournal(BABY_JOURNAL_2)

    description = analysis.describeProgress(e1)
    assert description == """
    """

    description2 = analysis.describeProgress(e2)
    assert description2 == """
    """


def test_UnexploredBranches():
    """
    Tests the `unexploredBranches` and `unexploredBranchesPerStep`
    functions.
    """
    ex = journal.convertJournal(JOURNAL)
    assert analysis.unexploredBranches(ex.getGraphAtStep(0)) == []
    assert analysis.unexploredBranches(ex.getGraphAtStep(1)) == [
        ('Start', 'right'),
        ('Start', 'left'),
    ]
    assert analysis.unexploredBranches(ex.getGraphAtStep(2)) == [
        ('Start', 'right'),
        ('coin_room', 'up'),
    ]
    assert analysis.unexploredBranches(ex.getGraphAtStep(3)) == [
        ('Start', 'right'),
        ('coin_room', 'up'),
    ]
    assert analysis.unexploredBranches(ex.getGraphAtStep(4)) == [
        ('coin_room', 'up'),
        ('platforms', 'up'),
        ('platforms', 'right'),
    ]
    assert analysis.unexploredBranches(ex.getGraphAtStep(5)) == [
        ('coin_room', 'up'),
        ('platforms', 'right'),
    ]
    perstep = analysis.unexploredBranchesPerStep(ex)
    assert perstep == [0, 2, 2, 2, 3, 2]


def test_averageBranches():
    """
    Tests the averageBranches function
    """

    ex = journal.convertJournal(BABY_JOURNAL)
    assert analysis.averageBranches(ex) == (
        "Total Nodes: 1 Current edges: 'Start(?)' Total edges: 1"
      + " Average at step: 1"
      + "Total Nodes: 2 Current egdes: 'up,'left' Total edges: 2"
      + " Average at step: 1"
      + "Total Nodes: 3 Current edges: 'up' Total edges: 5"
      + " Average at step: 0.6"
    )
