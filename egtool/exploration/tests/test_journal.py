"""
Authors: Peter Mawhorter
Consulted:
Date: 2022-9-30
Purpose: Tests for journal functionality.
"""

import json

import pytest

from .. import core, journal


def test_simpleConversion() -> None:
    """
    A simple test of journal conversion.
    """
    simple = journal.convertJournal("""\
S First_Room
x Exit Second_Room Entrance
gt tag

o Onwards
E END
""")
    assert len(simple) == 4

    finalGraph = simple.currentGraph()

    assert list(finalGraph["First_Room"]) == ["Second_Room"]
    assert sorted(finalGraph["Second_Room"]) == [
        "First_Room",
        "_e:END",
        "_u.1",
    ]
    assert sorted(finalGraph.nodes) == [
        "First_Room",
        "Second_Room",
        "_e:END",
        "_u.1",
    ]
    assert len(finalGraph) == 4
    assert json.dumps(
        finalGraph.textMapObj(),
        indent=4
    ) == """\
{
    "First_Room::Exit": {
        "Second_Room::Entrance": "First_Room",
        "Second_Room::Onwards": {
            "_u.1::return": "Second_Room"
        },
        "Second_Room::_e:END": {}
    }
}"""

# TODO: Specific case coverage tests...


def test_exploring_zones() -> None:
    """
    A test for exploration with zones.
    """
    expl = journal.convertJournal("""\
P on
S room zone
x ahead room2 back
o hatch basement ladder
x ahead room3 back zone2
t back
x hatch
""")
    assert expl.currentPosition() == 'zone::basement'
    now = expl.currentGraph()
    assert sorted(now) == [
        'zone2::room3',
        'zone::basement',
        'zone::room',
        'zone::room2',
    ]
    assert now.zoneParents('zone::room') == {'zone'}
    assert now.zoneParents('zone::room2') == {'zone'}
    assert now.zoneParents('zone2::room3') == {'zone2'}
    assert now.zoneParents('zone::basement') == {'zone'}


def test_startOfCotM() -> None:
    """
    A test for converting part of a journal from Castlevania: Circle of
    the Moon. This journal covers just the first few rooms of the game.
    """
    # Note that we're expecing a `TransitionBlockedWarning` because the
    # requirement 'q distance' does not match the power gained 'At gain
    # dash_boots'...
    with pytest.warns(core.TransitionBlockedWarning):
        expl = journal.convertJournal("""\
# Setup
# -----
P on  # Turns on zone-prefixing (we're using level-0 zones for rooms)
# A save-point alias that creates, explores and tags a new decision but
# then returns to where we just were.
= savePoint [
    x {_1} save{_2} return
    g save
    t return
]
= dagger [
    a candle{_}
    At gain dagger
    At lose axe
    At lose holy_water
    At lose cross
]
= axe [
    a candle{_}
    At lose dagger
    At gain axe
    At lose holy_water
    At lose cross
]
= holy_water [
    a candle{_}
    At lose dagger
    At lose axe
    At gain holy_water
    At lose cross
]
= cross [
    a candle{_}
    At lose dagger
    At lose axe
    At lose holy_water
    At gain cross
]

# Journal
# -------
S main Confrontation_Site  # Stars the exploration, and applies a zone
zz Catacombs  # Creates and applies a level-1 zone above the existing zone
o Top_Left  # Observes a transition at the current decision
x Bottom top Top Rubble_Tower_Base  # Explores a transition (newly created)
    # The 'x' entry above notes the transition taken, the room name on
    # the other end, the transition name for the reciprocal, and the
    # new level-0 zone for the destination (could be omitted if it's in
    # the same level-0 zone).
    gt forced  # Tags the transition we just explored

o Crack  # We see a crack in the wall as we go down
    q short_and_high  # But we can't get through it right now
    # Spoilers: we'll never be able to get through it from this side
n Fought -Skeleton_Bomber-  # This is a note, applied to the exploration step
A gain Salamander  # An applied effect: gaining a power
# TODO: Random drops
n Literally first enemy I touched! # Another note
x Bottom_Right top Top_Left Catacombs_Entrance  # Another exploration

o up
    n Can't jump up here yet
    q height
o top_stairs mid top_stairs  # The stairs to the middle level
    # Here we define the room name on the other side and the reciprocal
    # transition name.
x First_Right left Left Catacombs_Treasury  # But first a detour

o box  # Can't get through this box yet
    qb heavy  # Requirement applies in both directions
t Left  # This 't' retraces a known transition at the current position

x top_stairs  # Here the room and reciprocal names have already been
    # defined, and the zone is the same

o bottom_stairs bottom bottom_stairs
A gain Mercury
n Bone head along the way

> savePoint
x bottom_stairs
> dagger
o Bot_Left
o across_bottom
x Bot_Left bottom Right Blockwall_Cave

o ledge
  q height
t Right
x across_bottom bottom_right back_across_bottom
o Bot_Right
u bottom
# Nothing super interesting across the bottom of the room, so we might
# as well treat the entire bottom as one decision.
x Bot_Right entrance Left Green_Gates

n Fought -Earth_Demon-
o jump
  q distance
a get_dash_boots
At gain dash_boots
# TODO: power -> requirement mapping...
x jump upper left
o right_stairs_down right_side
o climb
x climb attic fall
v fall_left entrance fly_up
    qr flight
v fall_right right_side fly_up
    qr flight
x Upper_Right entrance Left Catacombs_Treasury_2
o block
    qb crumble
t Left
x fall_right
x Lower_Right main Left Zombie_Corridor

n [
    Fought -Poison_Worm- combo'd with -Zombie- and -Skeleton-. This
    makes for an interesting room.
]
# This annotation spans multiple lines of text
x Right bottom_left Left_Mid Pinkstone_Tower

# And that's the end of this fragment for testing purposes!
""")
    assert len(expl) == 23

    firstSituation = expl.situationAtStep(0)
    secondSituation = expl.situationAtStep(1)
    fifthSituation = expl.situationAtStep(5)
    finalSituation = expl.currentSituation()

    assert len(firstSituation.graph) == 0
    assert firstSituation.position is None

    assert len(secondSituation.graph) == 3  # room + 2 unknowns
    assert sorted(secondSituation.graph) == [
        'Confrontation_Site::main',
        '_u.0',
        '_u.1',
    ]
    assert secondSituation.position == "Confrontation_Site::main"

    g5 = fifthSituation.graph
    assert len(g5) == 9
    assert sorted(g5) == [
        'Catacombs_Entrance::mid',
        'Catacombs_Entrance::top',
        'Catacombs_Treasury::left',
        'Confrontation_Site::main',
        'Rubble_Tower_Base::top',
        '_u.0',
        '_u.2',
        '_u.4',
        '_u.7',
    ]
    assert g5.destination('_u.0', 'return') == 'Confrontation_Site::main'
    assert g5.destination('_u.2', 'return') == 'Rubble_Tower_Base::top'
    assert g5.destination('_u.4', 'return') == 'Catacombs_Entrance::top'
    assert g5.destination('_u.7', 'return') == 'Catacombs_Treasury::left'
    assert fifthSituation.position == "Catacombs_Entrance::top"

    gL = finalSituation.graph
    assert len(gL) == 21
    assert sorted(gL) == [
        'Blockwall_Cave::bottom',
        'Catacombs_Entrance::bottom',
        'Catacombs_Entrance::mid',
        'Catacombs_Entrance::save_1',
        'Catacombs_Entrance::top',
        'Catacombs_Treasury::left',
        'Catacombs_Treasury_2::entrance',
        'Confrontation_Site::main',
        'Green_Gates::attic',
        'Green_Gates::entrance',
        'Green_Gates::right_side',
        'Green_Gates::upper',
        'Pinkstone_Tower::bottom_left',
        'Rubble_Tower_Base::top',
        'Zombie_Corridor::main',
        '_u.0',
        '_u.12',
        '_u.2',
        '_u.22',
        '_u.4',
        '_u.7',
    ]
    assert finalSituation.position == "Pinkstone_Tower::bottom_left"


def _test_CotMToCerberus() -> None:
    """
    A test for converting part of a journal from Castlevania: Circle of
    the Moon. This journal covers an experienced re-playthrough up to
    the first boss (Cerberus) and boss reward (double-jump).

    TODO: This test is not finished yet!
    """
    expl = journal.convertJournal("""\
# Setup
# -----
P on  # Turns on zone-prefixing (we're using level-0 zones for rooms)
# A save-point alias that creates, explores and tags a new decision but
# then returns to where we just were.
= savePoint [
    x {_1} save{_2} return
    g save
    t return
]
= dagger [
    a candle{_}
    At gain dagger
    At lose axe
    At lose holy_water
    At lose cross
]
= axe [
    a candle{_}
    At lose dagger
    At gain axe
    At lose holy_water
    At lose cross
]
= holy_water [
    a candle{_}
    At lose dagger
    At lose axe
    At gain holy_water
    At lose cross
]
= cross [
    a candle{_}
    At lose dagger
    At lose axe
    At lose holy_water
    At gain cross
]

# Journal
# -------
S main Confrontation_Site  # Stars the exploration, and applies a zone
zz Catacombs  # Creates and applies a level-1 zone above the existing zone
o Top_Left  # Observes a transition at the current decision
x Bottom top Top Rubble_Tower_Base  # Explores a transition (newly created)
    # The 'x' entry above notes the transition taken, the room name on
    # the other end, the transition name for the reciprocal, and the
    # new level-0 zone for the destination (could be omitted if it's in
    # the same level-0 zone).
    gt forced  # Tags the transition we just explored

o Crack  # We see a crack in the wall as we go down
    q short_and_high  # But we can't get through it right now
    # Spoilers: we'll never be able to get through it from this side
n Fought -Skeleton_Bomber-  # This is a note, applied to the exploration step
A gain Salamander  # An applied effect: gaining a power
# TODO: Random drops
n Literally first enemy I touched! # Another note
x Bottom_Right top Top_Left Catacombs_Entrance  # Another exploration

o up
    n Can't jump up here yet
    q height
o top_stairs mid top_stairs  # The stairs to the middle level
    # Here we define the room name on the other side and the reciprocal
    # transition name.
x First_Right left Left Catacombs_Treasury  # But first a detour

o box  # Can't get through this box yet
    qb heavy  # Requirement applies in both directions
t Left  # This 't' retraces a known transition at the current position

x top_stairs  # Here the room and reciprocal names have already been
    # defined, and the zone is the same
o bottom_stairs bottom bottom_stairs
A gain Mercury
n Bone head along the way

> savePoint
x bottom_stairs
> dagger
o Bot_Left
o across_bottom
x Bot_Left bottom Right Blockwall_Cave

o ledge
  q height
t Right
x across_bottom bottom_right back_across_bottom
o Bot_Right
u bottom
# Nothing super interesting across the bottom of the room, so we might
# as well treat the entire bottom as one decision.
x Bot_Right entrance Left Green_Gates
n Fought -Earth_Demon-
o jump
  q distance
a get_dash_boots
At gain dash_boots
# TODO: power -> requirement mapping...
x jump upper left
o right_stairs_down right_side
o climb
x climb attic fall
v fall_left entrance fly_up
    qr flight
o fall_right right_side fly_up
    qr flight
x Upper_Right entrance Left Catacombs_Treasury_2
o block
    qb crumble
t Left
x fall_right
x Lower_Right main Left Zombie_Corridor

n [
    Fought -Poison_Worm- combo'd with -Zombie- and -Skeleton-. This
    makes for an interesting room.
]
# This annotation spans multiple lines of text
x Right bottom_left Left_Mid Pinkstone_Tower

# TODO: HERE

[Pinkstone_Tower]
< Left_Mid
? down
? up
? across
>< Left_Crumble [. <mp>]
@ down
> Bottom_Right

[Green_Shaft_Cave]
< Left
# -Earth_Demon-
. <ht>
x< up (tall_and_narrow)
> Left

[Pinkstone_Tower]
< Bottom_Right
- across
> Mid_Right

[Squeeze_Under]
< Right
. <hp>
> Right

[Pinkstone_Tower]
< Mid_Right
- up
? up2
> Upper_Left

[Pinkstone_Connector_Crack]
< Right
x< crack (short_and_high)
> Right

[Pinkstone_Tower]
< Upper_Left
- up2
? across2
? up3
>< Top_Left [. <mp>]
- up3
? Top
- across2
>< Top_Right [save]
> Top

[FlameMouth_Chamber]
< Bottom
? right
> Left

[Pinkstone_Tower_2]
< Entrance_Crack
. <axe>
? Right
>< Left [. <hp>]
> Right

[Pinkstone_Connector_Crack]
< Left
-> crack
> Right

[Pinkstone_Tower]
< Upper_Left
> Top

[FlameMouth_Chamber]
< Bottom
- right
? up
- right2
. <mp>
x< ledge (height)
- up
> Top_Left

[Muddy_Pinkstone]
< Right
? down
>< Top_Left [save]
- down
> Bottom_Left

[Greenstone_Double_Shaft]
< Top_Right
# -Gremlin-
>< Mid_Right [. <ht>]
? up
> Bottom_Left

[Mummies_Hallway]
< Right
# Mummy
. <holy water>
x< ledge (height)
> Right

[Greenstone_Double_Shaft]
< Bottom_Left
- up
?b Top_Left
> Mid_Left

[Hopper_Treasury]
< Right
# -Hopper-
. <mp>
> Right

[Greenstone_Double_Shaft]
< Mid_Left
> Top_Left {boss}

[Cerberus]
< Right {boss}
# -Cerberus-
>< Left [. <double> (/height|distance)]
> Right
""")
    assert len(expl) == 3

    firstSituation = expl.situationAtStep(0)
    fifthSituation = expl.situationAtStep(5)
    finalSituation = expl.currentSituation()

    assert len(firstSituation.graph) == 1
    assert firstSituation.position == "Confrontation_Site"

    assert len(fifthSituation.graph) == 10
    assert fifthSituation.position == "Catacombs_Entrance"

    assert len(finalSituation.graph) == 100
    assert finalSituation.position == "Cerberus"


def test_entry_types():
    pf = journal.ParseFormat()
    for ebits, expect in [
        (["o", "action"], ("observe", None, ["action"])),
        (["observe", "action"], ("observe", None, ["action"])),
        (["oa", "action"], ("observe", "actionPart", ["action"])),
        (["ot", "action"], ("observe", "transitionPart", ["action"])),
        (["observe@a", "action"], ("observe", "actionPart", ["action"])),
        (
            ["observe@action", "action"],
            ("observe", "actionPart", ["action"])
        ),
        (
            ["observe@actionPart", "action"],
            ("observe", "actionPart", ["action"])
        ),
    ]:
        assert pf.determineEntryType(ebits) == expect, ebits

    for broken in (
        ["obse"],
        ["obse", "action"],
        ["observe@madeUp"],
        ["oq"],
    ):
        with pytest.raises(journal.JournalParseError):
            assert pf.determineEntryType(broken) is None, broken


def test_long_entry_types():
    """
    Tests the use of long entry types.
    """
    expl = journal.convertJournal("""\
START start
zone Start
zone@zone Region
observe@action action
observe door
explore door room1 door Room1
tag@both blue
""")
    expl2 = journal.convertJournal("""\
S start
z Start
zz Region
oa action
o door
x door room1 door Room1
gb blue
""")
    assert expl == expl2

# TODO: Specific case coverage tests...
